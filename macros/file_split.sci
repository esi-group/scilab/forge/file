// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function pathmat = file_split ( name  )
  // Split the path into a matrix of strings.
  // 
  // Calling Sequence
  // pathmat = file_split ( name )
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // pathmat : a n-by-1 matrix of strings
  //
  // Description
  // Returns a matrox of strings whose elements are the path components in name.
  // The first element of the list will have the same path type as name. 
  // All other elements will be relative.
  // 
  // Examples
  // name = file_tempfile (  )
  // pathmat = file_split ( name )
  // //
  // pathmat = file_split ( "/foo/myfile.txt" )
  // expected = ["/";"foo";"myfile.txt"]
  // //
  // pathmat = file_split ( "C:\foo\myfile.txt" )
  // expected_Windows = ["C:\";"foo";"myfile.txt"]
  // expected_Linux = "C:\foo\myfile.txt"
  // //
  // pathmat = file_split ( "C:/foo/myfile.txt" )
  // expected_Windows = ["C:/";"foo";"myfile.txt"]
  // expected_Linux = ["C:";"foo";"myfile.txt"]
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  sepmat(1) = file_separator()
  sepmat(2) = file_canonicalsep ( )
  pathmat = []
  buff = ""
  firstcomp = %t
  for k = 1 : length(name)
    c = part(name,k:k)
    if ( or(c==sepmat) ) then
      if ( firstcomp & buff == "" ) then
        pathmat($+1) = c
        firstcomp = %f
      else
        pathmat($+1) = buff
      end
      buff = ""
    else
      buff = buff + c
    end
  end
  if ( buff <> "" ) then
    pathmat($+1) = buff
  end
  pathmat(pathmat=="")=[]
endfunction

