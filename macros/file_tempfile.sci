// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function tempfile = file_tempfile (  )
  // Returns the name of a temporary file name suitable for writing.
  // 
  // Calling Sequence
  // tempfile = file_tempfile (  )
  //
  // Parameters
  // tempfile : a 1-by-1 matrix of strings
  //
  // Description
  // The tempfile name is unique, and the file will be writable.
  // 
  // Examples
  // name = file_tempfile (  )
  // 
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  maxtries = 10
  characterSet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", ..
	     "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", ..
             "t", "u", "v", "w", "x", "y", "z", ..
	     "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  nrand_chars = 10
  tempfile_done = %f
  for k = 1 : maxtries
    // Computes a random file tail
    randindices = grand(1,nrand_chars,"uin",1,size(characterSet,"*"))
    randchars = characterSet(randindices)
    filetail = strcat(randchars)
    // Computes a full file and see if the file exists
    tempfile = fullfile(TMPDIR,filetail)
    if ( fileinfo(tempfile) == [] ) then
      // The file does not exist; OK
      tempfile_done = %t
      break
    end
  end
  if ( ~tempfile_done ) then
    error (msprintf("%s: Failed to generate a temporary file name","file_tempfile"))
  end
endfunction

