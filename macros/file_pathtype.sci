// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function t = file_pathtype ( name )
  // Returns the type of a path.
  // 
  // Calling Sequence
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // t : a 1-by-1 matrix of floating point integers, t=1 for absolute, t=2 for relative, t=3 for volumerelative.
  //
  // Description
  // Allows to get the type of a path.
  // If name refers to a specific file on a specific volume, the path
  // type will be absolute. 
  // If name refers to a file relative to the current
  // working directory, then the path type will be relative. 
  // If name refers to a file relative to the current working directory on a specified volume, or to
  // a specific file on the current working volume, then the path type is volumerelative.
  //
  // Volume relative can happen only on Windows.
  // 
  // For example:
  // <itemizedlist>
  // <listitem>"." is relative on all platforms</listitem>
  // <listitem>".." is relative on all platforms</listitem>
  // <listitem>"/" is absolute on Linux/Unix</listitem>
  // <listitem>"C:/" is absolute on Windows (if the C:/ exists)</listitem>
  // <listitem>"/" is volumerelative on windows and refers to the current volume (for example C:/)</listitem>
  // <listitem>"toto.txt" is relative on all platforms</listitem>
  // <listitem>"./toto.txt" is relative on all platforms </listitem>
  // </itemizedlist>
  //
  // Examples
  // name = file_tempfile (  )
  // file_touch ( name )
  // file_pathtype ( name ) // Should be 1
  // //
  // file_pathtype ( "foo.txt" ) // Should be 2
  // //
  // volumematrix = getdrives()
  // file_pathtype ( volumematrix(1) ) // Should be 1
  // //
  // file_pathtype ( "/" ) // Should be 3 on Windows, 1 on Linux
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  nativename = file_nativename ( name )
  firstsep = file_firstsepindex ( name )
  platform = getos ()
  if ( firstsep==[]) then
    t = 2
  elseif (firstsep==1) then
    // There is one separator, which is the first character
    if ( platform == "Windows" ) then
      t = 3
    else
      t = 1
    end
  else
    firstcomp = part(nativename,1:firstsep-1)
    if ( firstcomp=="." | firstcomp==".." ) then
      t = 2
    else
      //
      // The first item in the path is neither a ".", nor a ".."
      // and the first character is not a separator.
      // On Linux and Mac, this is a relative file.
      // But on Windows, "toto/titi.txt" is relative and
      // "C:/titi.txt" is absolute : we have to make the difference
      // between "toto", which is just a regular directory name,
      // and "C:", which is a file volume.
      //
      volumematrix = getdrives()
      firstcomp  = firstcomp  + "/"
      volumematrix = strsubst(volumematrix,"\","/")
      volumeindex = find(volumematrix==firstcomp)
      if ( volumeindex <> [] ) then
        t = 1
      else
        t = 2
      end
    end
  end
endfunction

