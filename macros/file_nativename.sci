// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function path = file_nativename ( name )
  // Returns the platform-specific name of a path.
  // 
  // Calling Sequence
  // path = file_nativename ( name )
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // path : a 1-by-1 matrix of strings
  //
  // Description
  // Returns the platform-specific name of a path.
  // 
  // Examples
  // tmpfile = file_tempfile (  )
  // path = file_nativename ( tmpfile )
  // //
  // name = file_nativename ( "\foo\myfile.txt" )
  // expected_Linux="/foo/myfile.txt";
  // expected_Windows="\foo\myfile.txt";
  // expected_Mac=":foo:myfile.txt";
  // //
  // name = file_nativename ( "/foo/myfile.txt" )
  // expected_Linux="/foo/myfile.txt";
  // expected_Windows="\foo\myfile.txt";
  // expected_Mac=":foo:myfile.txt";
  //
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  s = file_separator()
  path = strsubst(name,"\",s)
  path = strsubst(path,"/",s)
endfunction

