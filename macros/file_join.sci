// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function name = file_join ( pathmat )
    // Join sub-paths from a matrix of strings.
    // 
    // Calling Sequence
    // name = file_join ( pathmat )
    //
    // Parameters
    // pathmat : a n-by-1 matrix of strings
    // name : a 1-by-1 matrix of strings
    //
    // Description
    // Returns a path made by joining the paths, using the 
    // platform-specific canonical separator, that is 
    // "/" on Windows and Linux and ":" on Mac.
    // 
    // Examples
    // name = file_join ( ["foo" "myfile.txt" ] )
    // expected_WinLinux = "foo/myfile.txt"
    // expected_Mac = "foo:myfile.txt"
    // 
    // name = file_join ( ["C:\" "foo" "myfile.txt" ] )
    // expected_Windows = "C:\foo\myfile.txt"
    // expected_Linux = "C:\/foo/myfile.txt"
    // 
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (c) 2008 - Michael Baudin
    // Copyright (c) 2008 - Arjen Markus


    separator = file_canonicalsep ()
    //
    // 1. Add the directory name
    //
    name = pathmat(1)
    //
    // Join the remaining parts
    for k = 2 : size(pathmat,"*")
        //
        // If the file type is absolute, remove the directory
        //
        pathtype = file_pathtype ( pathmat(k) )
        if ( pathtype == 1 | pathtype == 3) then
            name = pathmat(k)
        else
            //
            // If the last character of the directory name is allready
            // a separator, do not insert a separator.
            // Otherwise, insert one.
            //
            lastsep = file_lastsepindex ( name )
            dirname_length = length ( name )
            if ( lastsep <> dirname_length ) then
                name = name + separator
            end
            name = name + pathmat(k)
        end
    end
endfunction

