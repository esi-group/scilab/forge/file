//
// This help file was automatically generated from file_isfile.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_isfile.sci
//

file_isfile ( filejoin([SCI,"etc","scilab.start"] ) // %t
file_isfile ( TMPDIR ) // %f
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_isfile.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
