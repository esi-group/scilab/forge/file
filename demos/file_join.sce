//
// This help file was automatically generated from file_join.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_join.sci
//

name = file_join ( ["foo" "myfile.txt" ] )
expected_WinLinux = "foo/myfile.txt"
expected_Mac = "foo:myfile.txt"
halt()   // Press return to continue
 
name = file_join ( ["C:\" "foo" "myfile.txt" ] )
expected_Windows = "C:\foo\myfile.txt"
expected_Linux = "C:\/foo/myfile.txt"
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_join.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
