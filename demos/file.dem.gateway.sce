// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("file.dem.gateway.sce");
subdemolist = [
"file_touch", "file_touch.sce"; ..
"file_join", "file_join.sce"; ..
"file_pathtype", "file_pathtype.sce"; ..
"file_normalize", "file_normalize.sce"; ..
"file_split", "file_split.sce"; ..
"file_isdirectory", "file_isdirectory.sce"; ..
"file_tempfile", "file_tempfile.sce"; ..
"file_separator", "file_separator.sce"; ..
"file", "file.sce"; ..
"file_isfile", "file_isfile.sce"; ..
"file_exists", "file_exists.sce"; ..
"file_nativename", "file_nativename.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
