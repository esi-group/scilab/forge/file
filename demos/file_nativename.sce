//
// This help file was automatically generated from file_nativename.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_nativename.sci
//

tmpfile = file_tempfile (  )
path = file_nativename ( tmpfile )
//
name = file_nativename ( "\foo\myfile.txt" )
expected_Linux="/foo/myfile.txt";
expected_Windows="\foo\myfile.txt";
expected_Mac=":foo:myfile.txt";
//
name = file_nativename ( "/foo/myfile.txt" )
expected_Linux="/foo/myfile.txt";
expected_Windows="\foo\myfile.txt";
expected_Mac=":foo:myfile.txt";
halt()   // Press return to continue
 
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_nativename.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
